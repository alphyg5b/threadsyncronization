package com.thread.util;

import java.util.ArrayList;
import java.util.Vector;

public class Queue {
	private Vector q;
	private int size;
	//private int front=-1;
	//private int rear=-1;
	int i=0;
	public Queue(){
		this.size = 0;
		q = new Vector();
	}
	
	public void addd(){
		if(size==5){
			synchronized(q){
				try {
					q.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		else{
			synchronized(q){
				q.add(i);
				System.out.println("Added : " + i);
				increment(i);
				q.notify();
				size++;
			}
		}
	}
	
	private void increment(int i) {
		this.i++;
		
	}
	
	public boolean empty(){
		return q.isEmpty();
	}

	public void gett(){
		if(q.isEmpty()){
				synchronized(q){
				try {
					q.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		else{
			
			synchronized (q) {
				System.out.println("Removed : " + q.get(0));
				q.remove(0);
				q.notifyAll();
				size--;
			}
		}
	}
}

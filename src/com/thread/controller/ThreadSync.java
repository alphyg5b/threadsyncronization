package com.thread.controller;

import java.util.Scanner;
import com.thread.entities.Consumer;
import com.thread.entities.Producer;
import com.thread.util.*;

public class ThreadSync {
	
	
	public static Consumer c;
	
	public static void main(String[] args){
		Queue q = new Queue();
		Scanner s = new Scanner(System.in);
			Producer p = new Producer(q);
			c = new Consumer(q);
			Thread pr = new Thread(p);
			Thread co = new Thread(c);
			pr.start();
			co.start();
			s.nextLine();
			p.stop();
			System.out.println("Producer stopped");
			c.stopped=true;
			//System.out.println("Conusmer stopped");
		
	}


}

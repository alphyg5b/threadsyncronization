package com.thread.entities;

import com.thread.util.*;

public class Producer implements Runnable{
	
	private Queue q;
	private boolean running=true;
	
	public Producer(final Queue q){
		this.q = q;
	}
	
	public void stop(){
		this.running = false;
	}
	
	public void run() {
		int i=0;
		try {
			while(running!=false){
					q.addd();
					Thread.sleep(2000);
					
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}

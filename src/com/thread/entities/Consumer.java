package com.thread.entities;

import com.thread.util.*;

public class Consumer implements Runnable{
	
	private Queue q;
	private boolean running = true;
	public boolean stopped=false;
	
	public Consumer(final Queue q){
		this.q = q;
	}
	
	public void stop(){
		this.running = false;
	}

	public void run() {
		
		try{
			while(running!=false){
				if(stopped==true){
					while(!q.empty()){
						q.gett();
						Thread.sleep(1000);
					}
					System.out.println("\nConsumer stopped");
					stop();
				}
				else{
					q.gett();
					Thread.sleep(1000);
				}
			} 
		}catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
